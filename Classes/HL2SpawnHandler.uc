//=========================================================================================================
// Handles custom spawn events. Users define spawn events in the config, each of which have an associated
// probability and conditions that must be met for the event to occur. Each event defines a group of zeds
// to spawn as well as the relative timings of their spawning. At regular intervals, each spawn event is
// checked, and if successful, is queued so that the group will spawn based on the timing defined in the
// event info.
//=========================================================================================================

class HL2SpawnHandler extends Object
	within HL2Mut
	config(HL2Monsters);

//I haven't used an enumerator for ProbType in SpawnEventInfo because enumerators aren't extendable. An
//array of names could be used instead, but for the time being two prob types is manageable with a byte.

//TODO: have an option to force an entire spawn group to be spawned regardless of whether it's too big to
//		fit in this wave; just increase AIRemaining.

//DEPRECATED: implementation currently removed.
/**
  * Whether the spawn queue should be cleared when the wave ends. If this is false, any queued spawns will
  * be carried over to the next wave. This could lead to a large clump of custom zed spawns at the start of
  * a wave, which is why we use potentially apply relative staggering. See ApplyRelativeStaggering.
  */
//var config bool bClearSpawnQueueOnWaveEnd;

//DEPRECATED: implementation currently removed.
/**
  * If queued spawns carry over from the previous wave, we may have a large clump of zeds that are now
  * ready to spawn (since time will have passed during trader time). Thus, we can apply relative staggering
  * such that the time difference between queued events is reapplied relative to the first queued event -
  * which will be spawned after the wave start delay.
  */
//var config bool bApplyRelativeStaggeringOnWaveStart;

/*********************************************************************************************************
 * Spawn probability
 *********************************************************************************************************/

/** The multiplier to apply to all spawn event probabilities. */
var config float GlobalProbabilityScale;

//TODO: now that custom spawns replace standard spawns, is this necessary?
/**
  * The percentage of zeds killed in the current wave for which we consider it to be 'late' in the wave.
  * This is used for applying the late wave taper, which reduces custom spawn probability towards the end
  * of the wave.
  */
var config float LateWaveProgress;

/**
  * Whether the game conductor can affect the spawn probability. The game conductor has an effect on the
  * spawn rate of standard zeds, so this can be used to keep custom spawns in sync with the match dynamic.
  */
var config bool bAllowGameConductorToAffectSpawnProbability;

/*********************************************************************************************************
 * Arbitration - spawn percentage
 *********************************************************************************************************/

//See AdjustProbabilityToFitDesiredCustomSpawnPercentage for details.

/** The desired percentage of custom spawns within a wave (custom spawns / total spawns). */
var config float CustomSpawnPercentage;

/**
  * Defines how tightly the desired percentage should be kept to. If the current custom spawn percentage is
  * within this tolerance, the spawn probability is not altered.
  */
var config float CustomSpawnPercentageTolerance;

/** Defines the severity of the steps taken to balance the ratio. */
var config float CustomSpawnPercentageBalanceStrength;

/**
  * The spawn percentage arbitration system was designed to prevent custom spawns from dominating standard
  * if the probabilities are too high. However, if this is true it can also be used to increase the
  * probability of custom events if they aren't happening often enough relative to standard spawns.
  */
var config bool bIncreaseProbIfBelowDesiredSpawnPercentage;

//DEBUG: these are used for debugging or monitoring purposes (through getall or displayall in the console).
var float DebugPercentage, DebugPercentageMult;

/*********************************************************************************************************
 * Standard spawn manager modifiers
 *********************************************************************************************************/

/** Whether standard spawns should be disabled. */
var config bool bDisableStandardSpawns;

/**
  * Whether the standard boss spawn should be disabled. If true, a boss spawn event should be listed so that
  * the match can be completed.
  */
var config bool bDisableStandardBossSpawn;

/** The multiplier for the standard spawn rate. */
var config float StandardSpawnRateMultiplier;

/** Multiplies the probability of custom spawn events occuring during the boss wave. */
var config float BossWaveSpawnRateMultiplier;

/**
  * Multiplies the maximum amount of zeds alive at once. This applies to both standard and custom spawns
  * for non-boss waves. BossWaveConcurrentZedCountMultiplier is used for the boss wave.
  */
var config float ConcurrentZedCountMultiplier;

/** Multiplies the maximum amount of zeds alive at once for the boss wave only. */
var config float BossWaveConcurrentZedCountMultiplier;

/** Multiplies the amount of zeds per wave. */
var config float WaveZedCountMultiplier;

/** Defines a basic equation for scaling of a property. */
struct ScaleInfo
{
	//Value = Const + Coef * Amount^Exp;
	var float Const, Coef, Exp;
};

/** Defines how the probability of spawn events occurring scales with the amount of players. */
var config ScaleInfo SpawnEventProbabilityPlayerScale;

/*********************************************************************************************************
 * Spawn events
 *********************************************************************************************************/

/**
  * Contains all information pertaining to the spawning of a group of the same zed.
  * 
  * Zed - the type of zed to spawn.
  * Num - the amount of that zed to spawn in the group.
  * Stagger - the delay between each individual zed is spawned. E.g. Stagger=0.2 means the second zed
  * 	will spawn 0.2 seconds after the first zed.
  */
struct SpawnEventGroup
{
	var class<KFPawn_Monster> Zed;
	var byte Num;
	var float Stagger;
};

/**
  * Contains all information pertaining to a spawn event - in several groups of zeds can be spawned.
  * 
  * Groups - an array of groups that are spawned within the one spawn event.
  * PlayersMin - the minimum amount of players required for this event to occur.
  * PlayersMax - the maximum amount of players allowed for this event to occur. PlayersMax <= 0 can be used
  * to have the player count unbounded from above.
  * Stagger - the delay between each individual group is spawned. E.g. Stagger=0.2 means Groups[1]
  * 	will spawn 0.2 seconds after Group[0].
  * Prob - the probability of this spawn event occuring per second or per zed kill (based on ProbType).
  * WaveMin - the minimum wave progress (WaveNum / WaveMax) for this spawn event to occur.
  * WaveMax - the maximum wave progress (WaveNum / WaveMax) for this spawn event to occur.
  * Delay - the delay between spawn events of this type. E.g. Delay=2.0 prevents this spawn event from
  * 	occurring again until at least 2.0 seconds after it previously occurred.
  * ProbType - what factor the probability is with respect to. E.g. time, amount of zed deaths, etc.
  */
struct SpawnEventInfo
{
	var array<SpawnEventGroup> Groups;
	var int PlayersMin, PlayersMax;
	var float Stagger, Prob, WaveMin, WaveMax, Delay;
	var byte ProbType;
};

/**
  * Contains all information necessary for the spawn queue.
  * 
  * Zed - the type of zed to spawn.
  * Num - the amount of zeds to spawn at once. This will always be 1 unless Stagger=0.
  * Time - the time when the zed (group) should be spawned.
  */
struct SpawnQueueInfo
{
	var class<KFPawn_Monster> Zed;
	var byte Num;
	var float Time;
};

/** The interval between custom spawning events. Defines how often SpawnTimer is called. */
var config float SpawnCheckInterval;

/** The list of possible spawn events, selected at random based on their probability in SpawnTimer. */
var config array<SpawnEventInfo> SpawnEvents;

/**
  * The time remaining for each spawn event until they can occur again. Note that each index corresponds to
  * the same index in SpawnEvents.
  */
var array<float> SpawnEventDelays;

/** The delay after the start of a non-boss wave before spawn events can occur. */
var config float SpawnEventWaveStartDelay;

/** The delay after the start of a boss wave before custom spawn events can occur. */
var config float SpawnEventBossWaveStartDelay;

/** Determines whether custom spawns should occur during the boss wave. */
var config bool bCanSpawnDuringBossWave;

/**
  * Whether dead players should be considered when checking if there are enough players for a spawn event to
  * occur. If this is true, the amount of players in the server is used. If this is false, only living
  * players are considered.
  */
var config bool bShouldSpawnEventConsiderDeadPlayers;

/**
  * The amount of check events that a queued spawn event has failed to spawn. See CheckQueuedSpawns and
  * HandleProblemSpawn for details.
  */
var byte ProblemSpawnTries;

/*********************************************************************************************************
 * Spawn queue
 *********************************************************************************************************/

/** The list of queued spawns. */
var array<SpawnQueueInfo> SpawnQueue;

/**
  * The maximum amount of zed spawns that can be queued. Note that the queue can be compressed if Stagger=0
  * in the spawn events.
  */
var config int SpawnQueueSize;

/**
  * The indices in the array on which the spawn queue is built which correspond to the front and back of
  * the queue, respectively.
  */
var int QueueFrontIndex, QueueBackIndex;

/*********************************************************************************************************
 * State variables
 *********************************************************************************************************/

//These variables are used to calculate the change/delta between spawn checks, which affect the probability
//of spawn events occurring.
/** The time when the last spawn check occurred. */
var float LastSpawnCheckTime;
/** The zed count when the last spawn check occurred. */
var int LastSpawnCheckZedCount;

/**
  * The time at the start of the current wave. The accuracy of this time depends on SpawnCheckInterval since
  * we check when a new wave starts TrackWaveNumber, called from SpawnTimer.
  */
var float WaveStartTime;

/** The amount of zeds that have spawned this wave from custom spawn events. */
var int CustomSpawnsThisWave;

/**
  * The wave number from the previous spawn check. Used to track when a new wave starts (see TrackWaveNumber
  * for details).
  */
var byte LastWaveNum;

/** Initialises the handler. This should be called before the handler is used. */
function InitHandler()
{
	SpawnEventDelays.Length = SpawnEvents.Length;
	LastWaveNum = -1;
	
	InitialiseSpawnQueue();
	
	ModifyStandardSpawnManager();
}

/** Modifies settings in the standard spawn manager (KFAISpawnManager) when it is created. */
function ModifyStandardSpawnManager()
{
	//Wait for the spawn manager to be ready.
	if(Game.SpawnManager == None)
	{
		SetTimer(0.01, false, nameof(ModifyStandardSpawnManager), self);
		return;
	}
	
	ModifyStandardSpawnRate();
	ModifyConcurrentZedCount();
	
	//We can't modify the zed count per wave here because the relevant variables are protected or const.
	//Thus, we manually update WaveTotalAI each wave.
}

/**
  * Modifies settings in the standard spawn manager (KFAISpawnManager) each wave. Some settings need to
  * modified each wave because they are reset each wave.
  */
function ModifyStandardSpawnManagerPerWave()
{
	//If the standard spawns this wave should be disabled.
	if((bDisableStandardSpawns && !Game.MyKFGRI.IsBossWave()) || (bDisableStandardBossSpawn &&
		Game.MyKFGRI.IsBossWave()))
	{
		//Prevent the standard spawn manager from being able to spawn zeds this wave for a very long time.
		Game.SpawnManager.TimeUntilNextSpawn = 1000000000.0;
	}
	
	//Don't modify the zed count (WaveTotalAI) for the boss wave, otherwise we can have multiple bosses
	//spawn in. WaveTotalAI should be 1 for the boss wave.
	if(!Game.MyKFGRI.IsBossWave())
	{
		ModifyWaveZedCount();
	}
	//If it's a boss wave and the concurrent zed count needs to be adjusted.
	else if(ConcurrentZedCountMultiplier != BossWaveConcurrentZedCountMultiplier)
	{
		//This is called at the start of the match, but we need to apply the boss wave multiplier.
		ModifyConcurrentZedCount();
	}
}

/** Modifies the standard spawn rate (within KFAISpawnManager). */
function ModifyStandardSpawnRate()
{
	local int i;
	local float Multiplier;
	
	Multiplier = Game.MyKFGRI.IsBossWave() ? BossWaveSpawnRateMultiplier : StandardSpawnRateMultiplier;
	
	if(StandardSpawnRateMultiplier != 1.0)
	{
		//TODO: we could probably modify KFMapInfo.WaveSpawnPeriod instead. Need to check when the
		//WorldInfo.GetMapInfo() reference is ready.
		
		for(i = 0; i < ArrayCount(Game.SpawnManager.EarlyWavesSpawnTimeModByPlayers); ++i)
		{
			Game.SpawnManager.EarlyWavesSpawnTimeModByPlayers[i] /= Multiplier;
		}
		
		for(i = 0; i < ArrayCount(Game.SpawnManager.LateWavesSpawnTimeModByPlayers); ++i)
		{
			Game.SpawnManager.LateWavesSpawnTimeModByPlayers[i] /= Multiplier;
		}
	}
}

/** Modifies the maximum amount of zeds alive at once. */
function ModifyConcurrentZedCount()
{
	local int i, j;
	local float Multiplier;
	
	Multiplier = Game.MyKFGRI.IsBossWave() ? BossWaveConcurrentZedCountMultiplier :
		ConcurrentZedCountMultiplier;
	
	for(i = 0; i < Game.SpawnManager.PerDifficultyMaxMonsters.Length; ++i)
	{
		for(j = 0; j < Game.SpawnManager.PerDifficultyMaxMonsters[i].MaxMonsters.Length; ++j)
		{
			Game.SpawnManager.PerDifficultyMaxMonsters[i].MaxMonsters[j] *= Multiplier;
		}
	}
}

/** Modifies the total amount of zeds in each wave. */
function ModifyWaveZedCount()
{
	Game.SpawnManager.WaveTotalAI *= WaveZedCountMultiplier;
	Game.MyKFGRI.AIRemaining = Game.SpawnManager.WaveTotalAI;
}

/**
  * Enable custom spawn events.
  * 
  * bCheckNow - whether we should check for spawn events right now or wait for the SpawnTimer to be ready.
  */
function EnableSpawning(optional bool bCheckNow = false)
{
	UpdateLastSpawnCheckVars();
	
	if(bCheckNow)
	{
		SpawnTimer();
	}
	
	SetTimer(SpawnCheckInterval, true, nameof(SpawnTimer), self);
}

/**
  * Disable custom spawn events.
  * 
  * bClearQueue - whether the spawn queue should be cleared.
  */
function DisableSpawning(optional bool bClearQueue = false)
{
	ClearTimer(nameof(SpawnTimer));
	
	if(bClearQueue)
	{
		ClearSpawnQueue();
	}
}

/** Initialises the spawn queue. Called when the handler is first created. */
function InitialiseSpawnQueue()
{
	//Clear the spawn queue in case it isn't empty. This will handle setting the front and back indices.
	ClearSpawnQueue();
}

function ClearSpawnQueue()
{
	SpawnQueue.Length = 0;
	SpawnQueue.Length = SpawnQueueSize;
	
	QueueFrontIndex = INDEX_NONE;
	QueueBackIndex = INDEX_NONE;
}

/** Handles checking for spawn events and updating variables relevant to spawn events. */
function SpawnTimer()
{	
	TrackWaveNumber();
	
	if(!AllowCustomSpawnEvents())
	{
		return;
	}
	
	UpdateSpawnEventDelays();
	
	CheckQueuedSpawns();
	
	AttemptEnqueueSpawnEvents();
	
	UpdateLastSpawnCheckVars();
}

/** Checks if the wave number has changed (i.e. a new wave has started). */
function TrackWaveNumber()
{
	if(Game.WaveNum != LastWaveNum)
	{
		LastWaveNum = Game.WaveNum;
		
		NotifyNextWave();
	}
}

/** Called when the current wave number increases. */
function NotifyNextWave()
{
	WaveStartTime = WorldInfo.TimeSeconds;
	
	CustomSpawnsThisWave = 0;
	
	//In the current implementation, there shouldn't be any spawns carried over from the last wave. If there
	//are, clear them.
	ClearSpawnQueue();
	
	//Reset spawn event delays - just in case some delays are very large and carried over trader time.
	ClearSpawnEventDelays();
	
	/*if(bClearSpawnQueueOnWaveEnd)
	{
		ClearSpawnQueue();
	}
	else if(bApplyRelativeStaggeringOnWaveStart)
	{
		ApplyRelativeStaggering();
	}*/
	
	ModifyStandardSpawnManagerPerWave();
}

/** Resets all spawn event delays to zero. */
function ClearSpawnEventDelays()
{
	local int i;
	
	for(i = 0; i < SpawnEventDelays.Length; ++i)
	{
		SpawnEventDelays[i] = 0.0;
	}
}

//DEPRECATED: implementation not currently used.
/**
  * For queued spawns carried over from the previous wave, update their spawn times so that they will occur
  * relative to each other the same as they would have in the previous wave. Remember that
  * WorldInfo.TimeSeconds will likely have increased to a value such that all queued spawns are 'ready' now.
  */
/*function ApplyRelativeStaggering()
{
	local float FirstSpawnTime, TimeShift;
	local int i;
	
	if(IsQueueEmpty())
	{
		return;
	}
	
	FirstSpawnTime = SpawnQueue[QueueFrontIndex].Time;
	
	//Shift all queued times such that the first queued spawn will occur when the wave start delay is
	//over, and all subsequent spawns maintain the relative timings.
	TimeShift = SpawnEventWaveStartDelay + WorldInfo.TimeSeconds - FirstSpawnTime;
	
	i = QueueFrontIndex;
	
	while(i != QueueBackIndex)
	{
		SpawnQueue[i].Time += TimeShift;
		
		//Increment the index, but wrap around to index 0 after the end of the array is reached.
		i = (i + 1) % SpawnQueueSize;
	}
}*/

/**
  * Returns whether custom spawn events can occur now. Similar to CanSpawnEvents but considers a broader
  * range of conditions (e.g. whether the trader is currently open).
  */
function bool AllowCustomSpawnEvents()
{
	//Don't spawn zeds during trader time and until the match has started.
	if(!Game.MyKFGRI.bMatchHasBegun || Game.MyKFGRI.bMatchIsOver || Game.MyKFGRI.bTraderIsOpen)
	{
		return false;
	}
	
	//Don't spawn zeds during the boss wave if we're not meant to.
	if(!bCanSpawnDuringBossWave && Game.MyKFGRI.IsBossWave())
	{
		return false;
	}
	
	//Don't spawn zeds right at the start of a wave - use a different delay if it's a boss wave.
	if(WorldInfo.TimeSeconds - WaveStartTime < (Game.MyKFGRI.IsBossWave() ? SpawnEventBossWaveStartDelay :
		SpawnEventWaveStartDelay))
	{
		return false;
	}
	
	return CanSpawnZeds();
}

/** Returns whether zeds can be enqueued or spawned currently. */
function bool CanSpawnZeds()
{
	if(Game.AIAliveCount >= Game.SpawnManager.GetMaxMonsters())
	{
		return false;
	}
	
	if(HasSpawnedEnoughThisWave())
	{
		return false;
	}
	
	return true;
}

/**
  * Returns whether enough zeds have been spawned this wave. Currently only considers the amount of zeds to
  * spawn in a wave.
  */
function bool HasSpawnedEnoughThisWave()
{
	//NOTE: Game.NumAISpawnsQueued is the amount of zeds spawned so far in this wave.
	//We don't restrict the total spawn count for the boss wave.
	return !Game.MyKFGRI.IsBossWave() && (Game.NumAISpawnsQueued + GetNumQueuedZeds() >=
		Game.SpawnManager.WaveTotalAI);
}

/**
  * Updates the delays for each spawn event based on the amount of time that has passed since the last
  * spawn check.
  */
function UpdateSpawnEventDelays()
{
	local float DeltaTime;
	local int i;
	
	DeltaTime = WorldInfo.TimeSeconds - LastSpawnCheckTime;
	
	for(i = 0; i < SpawnEventDelays.Length; ++i)
	{
		SpawnEventDelays[i] = FMax(0.0, SpawnEventDelays[i] - DeltaTime);
	}
}

/**
  * Checks whether queued spawns are ready to occur (i.e. enough time has passed), and if they are, spawns
  * the groups of zeds contained in the queued items.
  */
function CheckQueuedSpawns()
{
	local int PrevIndex, IterationsSinceLastIndexChange;
	
	//A spawn event is considered to be a problem if it cannot be entirely spawned within 5 attempts - all
	//within a single check event - with spawns still being possible. If the same event remains a problem
	//for 3 consecutive check events, it is discarded to prevent blocking the spawn queue indefinitely.
	
	PrevIndex = QueueFrontIndex;
	IterationsSinceLastIndexChange = 0;
	
	//Loop through the spawn queue until we find a spawn event that isn't ready yet, or we find a group that
	//refuses to spawn.
	while(!IsQueueEmpty() && IterationsSinceLastIndexChange < 5)
	{	
		//Attempt to spawn the next event.
		if(ShouldSpawnNextEvent())
		{
			HandleNextSpawnEvent();
		}
		else
		{
			//If we can't spawn the next event, then all following events won't be ready either.
			return;
		}
		
		//Track the index of the next spawn in the queue.
		if(QueueFrontIndex == PrevIndex)
		{
			++IterationsSinceLastIndexChange;
		}
		else
		{
			PrevIndex = QueueFrontIndex;
			IterationsSinceLastIndexChange = 0;
			ProblemSpawnTries = 0;
		}
	}
	
	//If we exited the spawn loop because of a group that refused to spawn.
	if(!IsQueueEmpty() && ShouldSpawnNextEvent())
	{
		HandleProblemSpawn();
	}
}

/** Handles queued spawn events that fail to spawn after several attempts. */
function HandleProblemSpawn()
{
	local SpawnQueueInfo SpawnInfo;
	
	if(bEnableDebug)
	{
		SpawnInfo = PeekNextSpawn();
		
		`log("<HL2SpawnHandler.HandleProblemSpawn> Encountered problem spawn: (Zed: "$SpawnInfo.Zed$
			", Num: "$SpawnInfo.Num$")", bEnableDebug, 'HL2');
	}
	
	++ProblemSpawnTries;
	
	//If this event has failed for several check events.
	if(ProblemSpawnTries >= 3)
	{
		`log("<HL2SpawnHandler.HandleProblemSpawn> Discarded problem spawn after 3 attempts.",
			bEnableDebug, 'HL2');
		
		ProblemSpawnTries = 0;
		
		//Discard the problem spawn and try again.
		Dequeue();
		CheckQueuedSpawns();
	}
}

/** Checks if each spawn event can occur, then enqueues them randomly based on their probability. */
function AttemptEnqueueSpawnEvents()
{
	local int i, Index;
	
	//Randomise which spawn event is checked first. This is important for systems which alter the
	//probability of a spawn event occurring, such as arbitration. E.g. if the custom spawns are below the
	//desired percentage of total spawns, probability is increased. If a spawn event occurs, any following
	//spawn event won't have the probability increased as much since more custom spawns have happened.
	//Randomising the order of spawn event checks lessens the impact of this (on average).
	Index = Rand(SpawnEvents.Length);
	
	for(i = 0; i < SpawnEvents.Length; ++i)
	{	
		if(ShouldSpawn(Index, SpawnEvents[Index].Prob, SpawnEvents[Index].ProbType))
		{
			EnqueueSpawnEvent(Index);
		}
		
		Index = (Index + 1) % SpawnEvents.Length;
	}
}

/** Update variables that are used to save the state of the last spawn check (e.g. time of last check). */
function UpdateLastSpawnCheckVars()
{
	LastSpawnCheckTime = WorldInfo.TimeSeconds;
	
	if(Game.MyKFGRI != None)
	{
		LastSpawnCheckZedCount = Game.MyKFGRI.AIRemaining;
	}
}

/** Returns whether the next queued spawn is ready (i.e. enough time has passed). */
function bool ShouldSpawnNextEvent()
{
	local SpawnQueueInfo SpawnInfo;
	
	if(!CanSpawnZeds())
	{
		return false;
	}
	
	SpawnInfo = PeekNextSpawn();
		
	return SpawnInfo.Time <= WorldInfo.TimeSeconds;
}

/** Handles spawning the next event in the queue. */
function HandleNextSpawnEvent()
{
	local SpawnQueueInfo SpawnInfo;
	
	//Don't dequeue because we might not be able to spawn the entire group, and the spawn may fail.
	SpawnInfo = PeekNextSpawn();
	
	ModifySpawnCount(SpawnInfo.Zed, SpawnInfo.Num);
	
	`log("Spawning "$SpawnInfo.Num$" of "$SpawnInfo.Zed.Name, bEnableDebug, 'HL2');
	
	SpawnZeds(SpawnInfo.Zed, SpawnInfo.Num);
}

/**
  * Checks if the zed type is special (needs to be handled differently) and delegates responsibility for
  * spawning to the appropriate function. SpawnSquad is the default spawning function for standard zeds.
  */
function SpawnZeds(class<KFPawn_Monster> Zed, byte Num)
{
	local bool bIsSpecialZed;
	
	//Check if this zed should be handled differently.
	bIsSpecialZed = HandleSpecialZedSpawn(Zed, Num);
	
	//If it's a special zed, then it will have been spawned in HandleSpecialZedSpawn.
	if(!bIsSpecialZed)
	{
		SpawnSquad(Zed, Num);
	}
}

/** Modifies the number of zeds spawned. */
function ModifySpawnCount(class<KFPawn_Monster> Zed, out byte Num)
{
	local int MaxMonsters;
	
	MaxMonsters = Game.SpawnManager.GetMaxMonsters();
	
	//Prevent exceeding MaxMonsters zeds alive at once.
	if(Game.AIAliveCount + Num > MaxMonsters)
	{
		`log("Modified spawn count from "$Num$" to "$MaxMonsters - Game.AIAliveCount, bEnableDebug,
			'HL2');
		Num = MaxMonsters - Game.AIAliveCount;
	}
}

/**
  * Checks if the input zed should be handled differently and delegates spawning responsibility to a
  * handler function if required. Returns whether the zed was 'special' and therefore has already been
  * handled/spawned.
  */
function bool HandleSpecialZedSpawn(class<KFPawn_Monster> Zed, byte Num)
{
	local byte SpawnCount;
	
	SpawnCount = 0;
	
	//TODO: might want to do ClassIsChildOf checks instead.
	switch(Zed)
	{
		case class'Combine_Strider':
			SpawnCount = HandleSpawnStrider(Num);
			break;
		
		case class'Combine_Gunship':
			SpawnCount = HandleSpawnGunship(Num);
			break;
		
		case class'Hunter_Chopper':
			SpawnCount = HandleSpawnChopper(Num);
			break;
		
		default:
			//This is not a (supported) special zed.
			return false;
	}
	
	if(SpawnCount > 0)
	{
		NotifyZedsSpawned(Zed, SpawnCount);
	}
	
	return true;
}

/** Spawns a group of zeds of the same type. */
function SpawnSquad(class<KFPawn_Monster> Zed, byte Num)
{
	local byte SpawnCount;
	local array<class<KFPawn_Monster> > Squad;
	
	Squad = ConstructSquad(Zed, Num);
	SpawnCount = byte(Game.SpawnManager.SpawnSquad(Squad));
	
	if(SpawnCount > 0)
	{
		NotifyZedsSpawned(Zed, SpawnCount);
	}
}

/** Returns an array of zeds of type Zed with length Num. */
function array<class<KFPawn_Monster> > ConstructSquad(class<KFPawn_Monster> Zed, byte Num)
{
	local array<class<KFPawn_Monster> > Squad;
	local byte i;
	
	Squad.Length = Num;
	
	for(i = 0; i < Num; ++i)
	{
		Squad[i] = Zed;
	}
	
	return Squad;
}

/** Called when a group of zeds is spawned. */
function NotifyZedsSpawned(class<KFPawn_Monster> Zed, byte Num, optional bool bFromQueue = true)
{
	//If this spawn event was queued, remove it from the queue.
	if(bFromQueue)
	{
		UpdateQueuedSpawn(Zed, Num);
	}
	
	UpdateZedCount(Num);
}

/**
  * Updates the first spawn event in the queue. Subtracts Num from the event's spawn count. If the entire
  * group was spawned, the event is removed from the queue, otherwise it remains for the rest to be spawned
  * when it is possible.
  */
function UpdateQueuedSpawn(class<KFPawn_Monster> Zed, byte Num)
{
	local SpawnQueueInfo SpawnInfo;
	
	SpawnInfo = PeekNextSpawn();
	
	if(IsQueueEmpty())
	{
		`log("<HL2SpawnHandler.UpdateQueuedSpawn> Spawn queue is empty.", bEnableDebug, 'HL2');
		return;
	}
	
	//If the spawned zed class doesn't match what we expect.
	if(Zed != SpawnInfo.Zed)
	{
		//Don't update the first queued spawn.
		`log("<HL2SpawnHandler.UpdateQueuedSpawn> Spawned zed doesn't match expected zed class from "$
			"the spawn queue: "$Zed.Name$" vs. "$SpawnInfo.Zed.Name, bEnableDebug, 'HL2');
		return;
	}
	
	//If we've spawned more than expected from the queued event.
	if(Num > SpawnInfo.Num)
	{
		`log("<HL2SpawnHandler.UpdateQueuedSpawn> Spawned more zeds than expected from the spawn queue: "
			$Num$"$ vs. "$SpawnInfo.Num, bEnableDebug, 'HL2');
	}
	
	if(Num >= SpawnInfo.Num)
	{
		//If we spawned all zeds in this queued event, remove the event from the queue.
		Dequeue();
	}
	else
	{
		//If only part of this queue event was spawned, subtract from the event's spawn count, but keep it
		//queued so the rest will be spawned later.
		ReduceNextSpawn(Num);
	}
}

/**
  * Reduces the amount of zeds to spawn in the first queued event.
  * 
  * Assumptions: Num is smaller than the spawn count in the first queued event and the spawn queue isn't
  * empty. This should be guaranteed prior to calling the function.
  */
function ReduceNextSpawn(byte Num)
{
	SpawnQueue[QueueFrontIndex].Num -= Num;
}

/** Called whenever a zed is spawned from a custom spawn event. */
function UpdateZedCount(byte Num)
{
	Game.NumAISpawnsQueued += Num;
	CustomSpawnsThisWave += Num;
}

function byte HandleSpawnStrider(byte Num)
{
	local int i, SpawnCount;
	
	SpawnCount = 0;
	
	for(i = 0; i < Num; ++i)
	{
		if(SpawnStrider())
		{
			++SpawnCount;
		}
	}
	
	return SpawnCount;
}

function byte HandleSpawnGunship(byte Num)
{
	local int i, SpawnCount;
	
	SpawnCount = 0;
	
	for(i = 0; i < Num; ++i)
	{
		if(SpawnChopper(true))
		{
			++SpawnCount;
		}
	}
	
	return SpawnCount;
}

function byte HandleSpawnChopper(byte Num)
{
	local int i, SpawnCount;
	
	SpawnCount = 0;
	
	for(i = 0; i < Num; ++i)
	{
		if(SpawnChopper(false))
		{
			++SpawnCount;
		}
	}
	
	return SpawnCount;
}

/** Returns whether a spawn event should occur now. */
function bool ShouldSpawn(int SpawnEventIndex, float Prob, byte ProbType)
{
	local float Delta;
	
	//Although this is checked in SpawnTimer, we need to check again for each individual spawn event. A
	//successful spawn event prior to this one may mean there are now enough zeds in the wave.
	if(!CanSpawnZeds())
	{
		return false;
	}
	
	//Wait for the delay for this spawn event to be over.
	if(SpawnEventDelays[SpawnEventIndex] > 0.0)
	{
		return false;
	}
	
	if(!SpawnEventPassesWaveCheck(SpawnEventIndex))
	{
		return false;
	}
	
	if(!SpawnEventPassesPlayerCheck(SpawnEventIndex))
	{
		return false;
	}
	
	//Determine how much the relevant variable has changed since the last check. The probability is scaled
	//based on this change.
	Delta = GetDelta(ProbType);
	
	return (FRand() <= GetModifiedSpawnProbability(SpawnEventIndex, Prob, Delta));
}

/** Returns whether a spawn event's wave number conditions are met. */
function bool SpawnEventPassesWaveCheck(int SpawnEventIndex)
{
	local float WaveProgress;
	
	//We subtract 1 from WaveMax since it includes the boss wave. E.g. for medium length, WaveMax = 8.
	WaveProgress = float(Game.WaveNum) / (Game.WaveMax - 1);
	
	//Only spawn within the valid wave range specified in the spawn event info.
	return WaveProgress >= SpawnEvents[SpawnEventIndex].WaveMin && (WaveProgress <=
		SpawnEvents[SpawnEventIndex].WaveMax || SpawnEvents[SpawnEventIndex].WaveMax <= 0);
}

/** Returns whether a spawn event's player count conditions are met. */
function bool SpawnEventPassesPlayerCheck(int SpawnEventIndex)
{
	local int PlayerCount;
	
	PlayerCount = GetPlayerCount();
	
	//Only spawn within the valid player count range specified in the spawn event info.
	return PlayerCount >= SpawnEvents[SpawnEventIndex].PlayersMin && (PlayerCount <=
		SpawnEvents[SpawnEventIndex].PlayersMax || SpawnEvents[SpawnEventIndex].PlayersMax <= 0);
}

/**
  * Returns a value that gauges how 'long' it has been since the last spawn check. This will modify spawn
  * probability in a non-linear fashion (see ExtrapolateProbabilityStep for details).
  */
function float GetDelta(byte ProbType)
{
	switch(ProbType)
	{
		case 0:
			//The amount of time that has passed.
			return FMin(SpawnCheckInterval, WorldInfo.TimeSeconds - LastSpawnCheckTime);
		
		case 1:
			//The amount of zeds that have been killed.
			return LastSpawnCheckZedCount - Game.MyKFGRI.AIRemaining;
		
		default:
			`log("<HL2SpawnHandler.GetDelta> Unhandled probability type: "$ProbType, bEnableDebug, 'HL2');
			return 0;
	}
}

/** Returns the modified spawn probability based on several factors. */
function float GetModifiedSpawnProbability(int SpawnEventIndex, float Prob, float Delta)
{
	if(Prob <= 0.0)
	{
		return 0.0;
	}
	
	Prob = FMin(1.0, Prob);
	
	ExtrapolateProbabilityStep(Prob, Delta);
	Prob *= GlobalProbabilityScale;
	
	//Don't apply the late wave taper to boss waves since WaveTotalAI isn't valid for boss waves.
	//TODO: we could adjust the probability based on the boss's health and/or phase.
	if(!Game.MyKFGRI.IsBossWave())
	{
		Prob *= GetLateWaveTaper();
	}
	
	Prob = GetSpawnProbabilityPlayerCountModifier(Prob);
	
	if(bAllowGameConductorToAffectSpawnProbability)
	{
		//CurrentSpawnRateModification gets smaller to increase spawn rate - it normally multiplies the time
		//between spawns. To apply this to the spawn probability (and thus, the spawn rate) we divide.
		Prob /= Game.GameConductor.CurrentSpawnRateModification;
	}
	
	ArbitrateSpawnProbability(SpawnEventIndex, Prob);
	
	return Prob;
}

/**
  * Modifies the probability of a spawn event to account for not checking spawn events every second or zed
  * death. This allows the probability of a spawn event occurring after n seconds or n zed deaths to
  * be largely independent of how often spawn events are checked.
  */
function ExtrapolateProbabilityStep(out float Prob, float Delta)
{
	//Consider the probability of the event occurring after n seconds to be:
	//Pr(n) = 1 - (1 - P)^n,
	//where P is the probability of the event occurring per second (for example).
	//If we check at time intervals of t instead of 1.0, then we have:
	//Pr(n) = 1 - (1 - F(P,t))^(n/t),
	//where F(P, t) is an unknown function of the probability per second and the time between checks.
	//We want to find F(P,t) such that both definitions of Pr(n) are identical, i.e., checking if the
	//random event should occur more frequently shouldn't affect the probability of it occurring.
	//We arrive at F(P,t) = 1 - (1 - P)^t. This is the function that should be applied to the probability
	//to account for the fact that we may not check for spawn events every second.
	Prob = 1.0 - (1.0 - Prob) ** Delta;
}

/**
  * Returns a multiplier to apply to the spawn event probability based on how many zeds are left in the
  * wave. This allows the custom spawn events to taper off as the wave is ending so that the wave doesn't
  * keep going.
  */
function float GetLateWaveTaper()
{
	local float WaveProgress;
	
	WaveProgress = 1.0 - float(Game.MyKFGRI.AIRemaining) / Game.SpawnManager.WaveTotalAI;
	
	if(LateWaveProgress < 1.0 && WaveProgress >= LateWaveProgress)
	{
		//This is a linear interpolation with domain [LateWaveProgress, 1.0] and range [0.0, 1.0], with a
		//negative gradient. In other words, the modifier is 1.0 at (and before) LateWaveProgress, but
		//steadily decreases to 0.0 for WaveProgress between LateWaveProgress and 1.0.
		//We use FMax in case AIRemaining hasn't been properly updated from custom spawns.
		return FMax(0.0, (1.0 - WaveProgress) / (1.0 - LateWaveProgress));
	}
	else
	{
		return 1.0;
	}
}

/** Returns the probability of a spawn event, modified based on the player count. */
function float GetSpawnProbabilityPlayerCountModifier(float Prob)
{
	local float NotProb;
	
	if(Prob <= 0.0)
	{
		return 0.0;
	}
	
	NotProb = 1.0 - Prob;
	
	//We modify NotProb instead of Prob. This is so that the probability cannot exceed 1.0 for large
	//multipliers. Large multipliers will bring NotProb closer to 0.0, so Prob will approach 1.0.
	Prob = 1 - NotProb ** CalculateScaleMod(SpawnEventProbabilityPlayerScale, GetPlayerCount() - 1);
	
	return Prob;
}

/**
  * Returns the number of players that the spawning system considers. Note that this can take on several
  * meanings, e.g. the number of living players or the total amount of players in a server.
  */
function int GetPlayerCount()
{
	return bShouldSpawnEventConsiderDeadPlayers ? Game.GetNumPlayers() : Game.GetLivingPlayerCount();
}

/** Calculates a value based on the input scale info with Amount as the independent variable. */
function float CalculateScaleMod(ScaleInfo Mod, int Amount)
{
	return Mod.Const + Mod.Coef * (Amount ** Mod.Exp);
}

/**
  * Applies modifiers to the probability with the intention to prevent custom spawn events from dominating
  * - or being dominated by - the standard spawn system.
  */
function ArbitrateSpawnProbability(int SpawnEventIndex, out float Prob)
{
	//TODO:
	//Potentially assign a configurable weight to each zed (standard and custom) which is included when
	//determining the total 'weights' of custom and standard spawns.
	//Need CustomMaxMonsters which is separate from standard MaxMonsters. Queued spawns will have to wait
	//until we are below the CustomMaxMonsters limit.
	
	//If standard spawns are disabled, there is no point comparing the amount of custom and standard spawns.
	if(!bDisableStandardSpawns || StandardSpawnRateMultiplier <= 0.0)
	{
		//Don't try to compare standard and custom spawns during the boss wave, especially since different
		//bosses have different minion summoning mechanics.
		if(!Game.MyKFGRI.IsBossWave())
		{
			AdjustProbabilityToFitDesiredCustomSpawnPercentage(SpawnEventIndex, Prob);
		}
	}
}

/**
  * Modifies the spawn probability based on the percentage of spawns in this wave that are from custom
  * spawn events. The probability is adjusted so as to bring the spawn percentage closer to the desired
  * value, i.e. CustomSpawnPercentage.
  */
function AdjustProbabilityToFitDesiredCustomSpawnPercentage(int SpawnEventIndex, out float Prob)
{
	local float Percentage, LowerBound, UpperBound, Mean, Deviation, Mult;
	local int Sign;
	
	if(Game.NumAISpawnsQueued < 10)
	{
		//If we haven't had many spawns yet, we can't make any valid assessment of the spawn percentage.
		return;
	}
	
	Percentage = float(CustomSpawnsThisWave) / Game.NumAISpawnsQueued;
	DebugPercentage = Percentage;
	
	//If the percentage is outside of the tolerance range around the desired percentage, adjust the spawn
	//probability. We use a Gaussian distribution with standard deviation given by the inverse of the
	//balance strength, and centred on the bounds of the tolerance range. E.g. if the current percentage is
	//below the desired percentage (with tolerance included), then the centre of the Gaussian distribution
	//is the lower bound of the tolerance range.
	
	//If the tolerance covers 
	if(CustomSpawnPercentageTolerance >= 1.0)
	{
		return;
	}
	
	//These bounds define the range of tolerated percentages.
	LowerBound = CustomSpawnPercentage * (1.0 - CustomSpawnPercentageTolerance);
	UpperBound = CustomSpawnPercentage * (1.0 - CustomSpawnPercentageTolerance);
	
	//TODO: take into account the amount of zeds that will be spawned in this event.
	
	if(Percentage > UpperBound)
	{
		//If we have spawned too many custom zeds, reduce the probability. The centre of the distribution
		//is the upper bound of the tolerated range of percentages.
		Mean = UpperBound;
		Sign = -1;
	}
	else if(Percentage < LowerBound)
	{
		if(!bIncreaseProbIfBelowDesiredSpawnPercentage)
		{
			DebugPercentageMult = 1.0;
			return;
		}
		
		//If we have not spawned enough custom zeds, increase the probability. The centre of the
		//distribution is the lower bound of the tolerated range of percentages.
		Mean = LowerBound;
		Sign = 1;
	}
	else
	{
		//Don't modify the probability if we're within the tolerated range of percentages.
		DebugPercentageMult = 1.0;
		return;
	}
	
	//The standard deviation in the Gaussian distribution.
	Deviation = 1.0 / CustomSpawnPercentageBalanceStrength;
	
	//Apply a multiplier sampled from a Gaussian distribution, based on how far we are from the
	//tolerated range of percentages.
	Mult = 2.71828 ** (Sign * 0.5 * ((Percentage - Mean) / Deviation) ** 2);
	Prob *= Mult;
	DebugPercentageMult = Mult;
}

/** Enqueues an entire spawn event. */
function EnqueueSpawnEvent(int SpawnEventIndex)
{
	local int i;
	local float GroupStagger;
	
	GroupStagger = 0;
	
	for(i = 0; i < SpawnEvents[SpawnEventIndex].Groups.Length; ++i)
	{
		EnqueueGroupSpawn(SpawnEvents[SpawnEventIndex].Groups[i], GroupStagger);
		GroupStagger += SpawnEvents[SpawnEventIndex].Stagger;
	}
	
	//Prevent this spawn event for a while.
	SpawnEventDelays[SpawnEventIndex] = SpawnEvents[SpawnEventIndex].Delay;
}

//We can't make Group an out parameter because we pass in dynamic array elements.
/**
  * Enqueues an entire spawn group.
  * 
  * Group - the spawn group to enqueue.
  * GroupStagger - the stagger time for the entire group. Individial staggers from within the group are
  * 	added to this.
  */
function EnqueueGroupSpawn(SpawnEventGroup Group, int GroupStagger)
{
	local int i;
	local float Stagger;
	
	Stagger = GroupStagger;
	
	`log("Enqueuing "$Group.Num$" of "$Group.Zed.Name, bEnableDebug, 'HL2');
	
	if(Group.Stagger > 0.0)
	{
		//If there is stagger within the group, we have to enqueue the items separately since they will
		//have different spawn times.
		
		for(i = 0; i < Group.Num; ++i)
		{
			EnqueueSpawn(Group.Zed, 1, Stagger);
			Stagger += Group.Stagger;
		}
	}
	else
	{
		//If there is no stagger within this group, we can spawn them all in one clump.
		EnqueueSpawn(Group.Zed, Group.Num, Stagger);
	}
}

/** Constructs and enqueues a spawn group of the same type of zed. */
function EnqueueSpawn(class<KFPawn_Monster> Zed, byte Num, float Stagger)
{
	local SpawnQueueInfo SpawnInfo;
	
	SpawnInfo.Zed = Zed;
	SpawnInfo.Num = Num;
	SpawnInfo.Time = WorldInfo.TimeSeconds + Stagger;
	
	`log("Constructed SpawnQueueInfo: ("$SpawnInfo.Zed$", "$SpawnInfo.Num$", "$SpawnInfo.Time$")"$
		" from Stagger: "$Stagger$" at time: "$WorldInfo.TimeSeconds, bEnableDebug, 'HL2');
	
	Enqueue(SpawnInfo);
}

/** Adds an item to the back of the queue. */
function Enqueue(out SpawnQueueInfo SpawnInfo)
{
	if(QueueFrontIndex == QueueBackIndex)
	{
		//If we have no items in the queue.
		if(QueueFrontIndex == INDEX_NONE)
		{
			QueueFrontIndex = 0;
			QueueBackIndex = 0;
		}
		//If we have one item in the queue.
		else if(QueueFrontIndex == 0)
		{
			QueueBackIndex = 1;
		}
	}
	//If the back index is the last element of the list.
	else if(QueueBackIndex == SpawnQueueSize - 1)
	{
		//If the front index is the first element in the list.
		if(QueueFrontIndex == 0)
		{
			//We're full.
			`log("<HL2SpawnHandler.Enqueue> Unable to enqueue spawn for zed: "$SpawnInfo.Zed$
				", as the spawn queue is full.", bEnableDebug, 'HL2');
			return;
		}
		
		//Wrap around to the start of the list. This is what makes the queue circular.
		QueueBackIndex = 0;
	}
	else
	{
		//The only non-edge case. Simply move the back index further back one position.
		++QueueBackIndex;
	}
	
	//Add the spawn info to the back of the queue.
	SpawnQueue[QueueBackIndex] = SpawnInfo;
}

/** Returns and removes the spawn at the front of the spawn queue. */
function SpawnQueueInfo Dequeue()
{
	local SpawnQueueInfo SpawnInfo, EmptySpawnInfo;
	
	if(QueueFrontIndex == INDEX_NONE)
	{
		return EmptySpawnInfo;
	}
	
	//Retrieve the item at the first index.
	SpawnInfo = SpawnQueue[QueueFrontIndex];
	//Clear the item at the first index.
	SpawnQueue[QueueFrontIndex] = EmptySpawnInfo;
	
	//If we only have one item in the queue.
	if(QueueFrontIndex == QueueBackIndex)
	{
		QueueFrontIndex = INDEX_NONE;
		QueueBackIndex = INDEX_NONE;
	}
	//If the front index is the last element of the list.
	else if(QueueFrontIndex == SpawnQueueSize - 1)
	{
		QueueFrontIndex = 0;
	}
	else
	{
		//The only non-edge case. Simply move the front index further back one position.
		++QueueFrontIndex;
	}
	
	return SpawnInfo;
}

/** Retrieves the item at the front of the queue. */
function SpawnQueueInfo PeekNextSpawn()
{
	local SpawnQueueInfo SpawnInfo;
	
	if(IsQueueEmpty())
	{
		return SpawnInfo;
	}
	
	return SpawnQueue[QueueFrontIndex];
}

/**
  * Called when a queued spawn is too large to occur all at once (based on the maximum amount of zeds alive
  * at once), i.e. NumSpawned zeds are spawned, where NumSpawned is smaller than the amount of zeds in the
  * next queued spawn.
  */
function PartialSpawnNextEvent(int NumSpawned)
{
	SpawnQueue[0].Num -= NumSpawned;
}

function bool IsQueueEmpty()
{
	return QueueFrontIndex == INDEX_NONE;
}

/** Returns the total amount of queued zeds to spawn. */
function int GetNumQueuedZeds()
{
	local int Index, Size;
	
	if(IsQueueEmpty())
	{
		return 0;
	}
	
	Index = QueueFrontIndex;
	Size = 0;
	
	//Loop through all queued spawns, and accumulate the amount of zeds to spawn.
	
	while(Index != QueueBackIndex)
	{
		Size += SpawnQueue[Index].Num;
		Index = (Index + 1) % SpawnQueueSize;
	}
	
	//Add the back index since we exited the loop when this was encountered.
	Size += SpawnQueue[QueueBackIndex].Num;
	
	return Size;
}